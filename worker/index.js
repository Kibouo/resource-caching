const WORKER_URL_REGEX = /https:\/\/resource-caching.mihaly-csonka.workers.dev/;
const EDM_URL          = "http://webperf.wicability.net";
const EDM_PORT_HEADER  = "ndaport";
const EDM_PORT         = 8039;

const EXTREME_MAX_AGE = 60 * 5;

// header constants as I'm sure I'd make a typo *somewhere*
const CACHE_CONTROL_HEADER     = "Cache-Control";
const IF_NONE_MATCH_HEADER     = "If-None-Match";
const IF_MODIFIED_SINCE_HEADER = "If-Modified-Since";
const E_TAG_HEADER             = "ETag";
const LAST_MODIFIED_HEADER     = "Last-Modified";
const LOCATION_HEADER          = "Location";
const RANGE_HEADER             = "Range";

// circumvent Cloudflare `fetch` caching
const CUSTOM_CACHE_CONTROL_HEADER = "Custom-Cache-Control";
// in case Cloudflare's caching also looks at the `Expires` header
const CUSTOM_EXPIRES_HEADER     = "Custom-Expires";
const CUSTOM_FETCHED_AT_HEADER  = "Custom-Fetch-At";
const CUSTOM_VARY_HEADER        = "Custom-Vary";
const CUSTOM_PRUNE_CACHE_HEADER = "Custom-Prune-Cache";

const NO_STORE_DIRECTIVE = "no-store";
const PRIVATE_DIRECTIVE  = "private";

const NO_CACHE_DIRECTIVE               = "no-cache";
const MAX_AGE_DIRECTIVE                = "max-age";
const MIN_FRESH_DIRECTIVE              = "min-fresh";
const STALE_WHILE_REVALIDATE_DIRECTIVE = "stale-while-revalidate";
const MUST_REVALIDATE_DIRECTIVE        = "must-revalidate";
const ONLY_IF_CACHED_DIRECTIVE         = "only-if-cached";
const IMMUTABLE_DIRECTIVE              = "immutable";

const OK_STATUS                    = 200;
const PARTIAL_CONTENT_STATUS       = 206;
const MOVED_PERMANENTLY_STATUS     = 301;
const NOT_MODIFIED_STATUS          = 304;
const NOT_FOUND_STATUS             = 404;
const RANGE_NOT_SATISFIABLE_STATUS = 416;
const SERVER_ERROR_STATUS          = 500;
const GATEWAY_TIMEOUT_STATUS       = 504;

// Cache wrapper which handles multiple replies due to conneg
class ConnegCache
{
    constructor(cache) { this.cache = cache; }

    // normalizes `Accept-*` header value such that it can be semantically
    // compared to others. For example `en-US,en;q=0.5` is equivalent to
    // `en;q=0.5,en-US;q=1.0`.
    __normalize_accept_header_data(data_string)
    {
        const tuples = data_string.replace(/\s/g, "").split(",").map(
            tup => tup.split(";"));
        const item_with_quality = tuples.map(
            tup => [tup[0],
                    tup.length === 2 ? Number(tup[1].split("=")[1]) : 1]);
        return item_with_quality.sort((a, b) => {
            // sort by quality first
            if (a[1] > b[1]) { return -1; }
            else if (a[1] < b[1])
            {
                return 1;
            }
            // sort alphabetically on quality collision
            else
            {
                return a[0] < b[0] ? -1 : (a[0] === b[0] ? 0 : 1);
            }
        });
    }

    // To support multiple possible responses in the cache, secondary keys are
    // needed. `cache.default` only supports (Request,Response)-tuples, so we
    // solve this by simply appending the headers used in the Vary header to the
    // key (i.e. url).
    __expand_key(key, request, response)
    {
        const vary_header = response.headers.get(CUSTOM_VARY_HEADER);
        // according to RFC: don't match ever if `Vary` header's value is `*/*`.
        // This is because the origin used data outside of the generic headers
        // to do the content negotiation.
        if (vary_header === "*/*") { return null; }

        const headers_used_for_conneg
            = vary_header?.replace(/\s/g, "").split(",") || [];
        for (const header_name of headers_used_for_conneg)
        {
            const header = request.headers.get(header_name);
            key += header ? this.__normalize_accept_header_data(header) : "";
        }

        return key;
    }

    // according to RFC we should check the cache for potential replies to the
    // request. If the answers contain a `Vary` header, it is only valid if the
    // mentioned headers from the original cached request match the current
    // request (after normalization).
    async match(key, request)
    {
        const response_with_vary_header = await this.cache.match(key);
        if (!response_with_vary_header)
        {
            // no match
            return response_with_vary_header;
        }

        const vary_header
            = response_with_vary_header.headers.get(CUSTOM_VARY_HEADER);
        if (!vary_header)
        {
            // response might not actually contain a `Vary` header, in which
            // case no further matching is needed and the response satisfies the
            // request
            return response_with_vary_header;
        }

        return await this.cache.match(
            this.__expand_key(key, request, response_with_vary_header));
    }

    async put(key, request, response)
    {
        // update primary key in case origin suddenly decides to do content
        // negotiation for this resource based on other headers
        const update_primary_key_response
            = this.cache.put(key, response.clone());

        const vary_header = response.headers.get(CUSTOM_VARY_HEADER);
        // prevent double work (storing)
        if (!vary_header) { return await update_primary_key_response; }

        const update_expanded_key_response = this.cache.put(
            this.__expand_key(key, request, response), response);

        return await Promise.all(
            [ update_primary_key_response, update_expanded_key_response ]);
    }

    async delete(key, request)
    {
        const response_with_vary_header = await this.cache.match(key);
        if (!response_with_vary_header)
        {
            return;    // no match
        }

        const vary_header
            = response_with_vary_header.headers.get(CUSTOM_VARY_HEADER);
        if (!vary_header) { return await this.cache.delete(key); }

        return await Promise.all([
            this.cache.delete(key),
            this.cache.delete(
                this.__expand_key(key, request, response_with_vary_header))
        ]);
    }

    async targeted_delete(key, etag)
    {
        const potential_item = await this.cache.match(key);
        if (!potential_item)
        {
            return;    // no match
        }

        console.log(`selected delete target has ETag: ${
            potential_item.headers.get(
                E_TAG_HEADER)}. We want to delete ${etag}`);

        if (potential_item.headers.get(E_TAG_HEADER) === etag)
        {
            return await this.cache.delete(key);
        }
    }
}

addEventListener("fetch",
                 event => { event.respondWith(handleRequest(event)); });

function handle_edm_setup(client_request)
{
    client_request = new Request(client_request);    // make mutable
    client_request.headers.set(EDM_PORT_HEADER, EDM_PORT);
    return client_request;
}

async function fetch_origin_copy(url, client_request, etag = null, date = null)
{
    if (etag) { client_request.headers.set(IF_NONE_MATCH_HEADER, etag); }
    if (date)
    {
        client_request.headers.set(IF_MODIFIED_SINCE_HEADER,
                                   (new Date(date)).toUTCString());
    }

    let origin_response = await fetch(url, client_request);
    // make mutable
    origin_response = new Response(origin_response.body, origin_response);
    return [ origin_response, new Date() ];
}

function filter_cache_control_directives(from_cache_control_directives,
                                         to_cache_control_directives,
                                         directives_to_filter)
{
    for (const directive of directives_to_filter)
    {
        const directive_idx = from_cache_control_directives.findIndex(
            dir => dir.match(new RegExp(directive)));
        if (directive_idx > -1)
        {
            console.log(
                `${directive}: removing directive and handling it ourselves`);

            const directive_value
                = from_cache_control_directives[directive_idx];

            from_cache_control_directives.splice(directive_idx, 1);
            to_cache_control_directives.push(directive_value);
        }
    }

    return [ from_cache_control_directives, to_cache_control_directives ];
}

async function process_and_cache_response(
    client_request, origin_response, event, cache, cache_key, fetched_at)
{
    let cache_control_directives
        = origin_response.headers.get(CUSTOM_CACHE_CONTROL_HEADER)
              ?.replace(/\s/g, "")
              .split(",")
          || [];
    console.log("cache control directives: ", cache_control_directives);
    let our_cache_control_directives = [];    // stuff we handle ourselves

    if (cache_control_directives.includes(PRIVATE_DIRECTIVE)
        || cache_control_directives.includes(NO_STORE_DIRECTIVE))
    {
        console.log(`${PRIVATE_DIRECTIVE}|${NO_STORE_DIRECTIVE}: not caching`);
        return origin_response;
    }

    [cache_control_directives, our_cache_control_directives]
        = filter_cache_control_directives(
            cache_control_directives, our_cache_control_directives, [
                NO_CACHE_DIRECTIVE,
                MAX_AGE_DIRECTIVE,
                MIN_FRESH_DIRECTIVE,
                STALE_WHILE_REVALIDATE_DIRECTIVE,
                MUST_REVALIDATE_DIRECTIVE,
                ONLY_IF_CACHED_DIRECTIVE,
                IMMUTABLE_DIRECTIVE,
            ]);

    // we handle `max-age` ourselves as most interesting stuff happens after
    // an item is stale. However, we abuse the Cloudflare cache's
    // auto-remove-when-stale feature to keep everything clean.
    cache_control_directives.push(`${MAX_AGE_DIRECTIVE}=${EXTREME_MAX_AGE}`);
    origin_response.headers.set(CACHE_CONTROL_HEADER,
                                cache_control_directives.join(","));
    origin_response.headers.set(CUSTOM_CACHE_CONTROL_HEADER,
                                our_cache_control_directives.join(","));
    origin_response.headers.set(CUSTOM_FETCHED_AT_HEADER, fetched_at);

    console.log("headers to be cached: ", ...origin_response.headers.entries());
    event.waitUntil(cache.put(cache_key,
                              client_request.clone(),
                              origin_response.clone()));    // queue caching

    return origin_response;
}

async function update_cache_with_304(client_request,
                                     origin_response,
                                     event,
                                     cache,
                                     cache_key,
                                     fetched_at,
                                     cached_response)
{
    // make mutable
    cached_response = new Response(cached_response.body, cached_response);

    for (const [header_name, header_value] of origin_response.headers)
    {
        cached_response.headers.set(header_name, header_value);
    }

    // new custom headers will override the old ones
    return await process_and_cache_response(
        client_request, cached_response, event, cache, cache_key, fetched_at);
}

// This handles our custom prune-item functionality. The origin sets a
// `Custom-Prune-Cache` header which contains a list of items to prune. The
// header is preferably set on every response such that the CDN knows about
// it ASAP. The header's value is a comma-separated list of items to prune.
// The items have following syntax: `<path>;<ETag>`. This allows precise
// pruning of items.
function handle_prune_cache_functionality(response, event, cache)
{
    const prune_header = response.headers.get(CUSTOM_PRUNE_CACHE_HEADER);
    if (!prune_header) { return; }

    for (const item of prune_header.replace(/\s/g, "").split(","))
    {
        const [path, etag] = item.split(";");
        // we don't have to worry about multiple cached responses due to
        // content negotiation. By removing the primary response only, the
        // CDN will be forced to refresh this item. The refresh will then
        // overwrite the secondary response.
        event.waitUntil(cache.targeted_delete(EDM_URL + "/" + path, etag));
    }
}

async function fetch_and_cache(url,
                               client_request,
                               event,
                               cache,
                               cache_key,
                               cached_response = null,
                               fetch_etag      = null,
                               fetch_date      = null)
{
    let [response, fetched_at] = await fetch_origin_copy(
        url, client_request.clone(), fetch_etag, fetch_date);

    switch (response.status)
    {
        case OK_STATUS:
            await            cache.delete(cache_key, client_request);
            response = await process_and_cache_response(
                client_request, response, event, cache, cache_key, fetched_at);
            break;

        // permanently moved is checked as it's... well, permanent. 302,
        // however, is a temporary redirect. Caching it would not be useful
        // as the (request, redirection result)-tuple could become
        // deprecated at any time.
        case MOVED_PERMANENTLY_STATUS:
            console.log(
                `${response.status}: moved permanently, redirecting...`);
            // recursion/redirect depth should be checked in production code
            response
                = await fetch_and_cache(response.headers.get(LOCATION_HEADER),
                                        client_request,
                                        event,
                                        cache,
                                        cache_key,
                                        cached_response,
                                        fetch_etag,
                                        fetch_date);
            break;

        case NOT_MODIFIED_STATUS:
        {
            console.log(`${
                response
                    .status}: not modified. Updating cache item's headers and returning it.`);
            response = await update_cache_with_304(client_request,
                                                   response,
                                                   event,
                                                   cache,
                                                   cache_key,
                                                   fetched_at,
                                                   cached_response);
            break;
        }

        // 206s are responses to Range headers. Stitching is
        // expensive so no caching is done.
        case PARTIAL_CONTENT_STATUS:
            console.log(`${response.status}: passing on without caching.`);
            response.headers.set(CACHE_CONTROL_HEADER, NO_STORE_DIRECTIVE);
            response.headers.set(CUSTOM_CACHE_CONTROL_HEADER,
                                 NO_STORE_DIRECTIVE);
            // we simply act as a no-op proxy for ranged requests. We didn't
            // even check cache validity for the request, so we have no
            // right to delete a potentially matching item from the cache.
            break;

        // 404s are usually the result of a typo or old link. Thus, the
        // number of hits per url is low. This means caching is useless.
        // All other status codes are also treated as non-cacheable
        // (allow-list approach)
        case NOT_FOUND_STATUS:
        case SERVER_ERROR_STATUS:
        case RANGE_NOT_SATISFIABLE_STATUS:
        default:
            console.log(`${response.status}: not caching this.`);
            response.headers.set(CACHE_CONTROL_HEADER, NO_STORE_DIRECTIVE);
            response.headers.set(CUSTOM_CACHE_CONTROL_HEADER,
                                 NO_STORE_DIRECTIVE);
            event.waitUntil(cache.delete(cache_key, client_request.clone()));
            break;
    }

    handle_prune_cache_functionality(response.clone(), event, cache);
    // end-users shouldn't know about which files contain sensitive info :)
    response.headers.delete(CUSTOM_PRUNE_CACHE_HEADER);

    return response;
}

function determine_ms_before_stale(response)
{
    const cache_control_directives
        = response.headers.get(CUSTOM_CACHE_CONTROL_HEADER)
              ?.replace(/\s/g, "")
              .split(",")
          || [];

    const max_age_idx = cache_control_directives.findIndex(
        dir => dir.match(new RegExp(MAX_AGE_DIRECTIVE)));
    if (max_age_idx > -1)
    {
        const current_age
            = (new Date())
              - (new Date(response.headers.get(CUSTOM_FETCHED_AT_HEADER)));
        const max_age
            = cache_control_directives[max_age_idx].split("=")[1] * 1000;
        return max_age - current_age;
    }

    // if no `max-age` present, try the backup `Expires` header
    const expires = response.headers.get(CUSTOM_EXPIRES_HEADER);
    if (expires) { return (new Date(expires)) - (new Date()); }

    return 0;
}

function handle_no_cache(response_cache_control_directives)
{
    if (response_cache_control_directives.includes(NO_CACHE_DIRECTIVE))
    {
        console.log(
            `${NO_CACHE_DIRECTIVE}: checking for freshness with origin`);
        return [ true, false ];
    }

    return null;
}

function handle_min_fresh(request_cache_control_directives, ms_before_stale)
{
    const min_fresh_idx = request_cache_control_directives.findIndex(
        dir => dir.match(new RegExp(MIN_FRESH_DIRECTIVE)));
    if (min_fresh_idx > -1)
    {
        const min_fresh = Math.max(
            request_cache_control_directives[min_fresh_idx].split("=")[1]
                * 1000,
            0);

        if (ms_before_stale < min_fresh)
        {
            console.log(`${MIN_FRESH_DIRECTIVE}: stale in ${
                ms_before_stale}ms but want at least ${
                min_fresh}ms. Contacting origin...`);
            return [ true, false ];
        }
    }

    return null;
}

function handle_must_revalidate(response_cache_control_directives,
                                ms_before_stale)
{
    const must_revalidate_idx = response_cache_control_directives.findIndex(
        dir => dir.match(new RegExp(MUST_REVALIDATE_DIRECTIVE)));
    if (must_revalidate_idx > -1)
    {
        console.log(`${
            MUST_REVALIDATE_DIRECTIVE}: forced checking whether item is stale, before other directive can decide that staleness is fine`);
        return handle_max_age(ms_before_stale);
    }

    return null;
}

function handle_stale_while_revalidate(request_cache_control_directives,
                                       ms_before_stale)
{
    const stale_while_revalidate_idx
        = request_cache_control_directives.findIndex(
            dir => dir.match(new RegExp(STALE_WHILE_REVALIDATE_DIRECTIVE)));
    if (stale_while_revalidate_idx > -1)
    {
        const max_stale
            = request_cache_control_directives[stale_while_revalidate_idx]
                  .split("=")[1]
              * 1000;

        if (ms_before_stale <= 0)
        {
            if (max_stale + ms_before_stale >= 0)
            {
                console.log(`${
                    STALE_WHILE_REVALIDATE_DIRECTIVE}: item is stale for ${
                    ms_before_stale * -1}, but hasn't passed the max of ${
                    max_stale}. Returning cache and contacting origin async...`);
                return [ true, true ];
            }
            else
            {
                console.log(`${
                    STALE_WHILE_REVALIDATE_DIRECTIVE}: item stale for too long. Contacting origin async...`);
                return [ true, false ];
            }
        }
        // else not stale
    }

    return null;
}

function handle_max_age(ms_before_stale)
{
    if (ms_before_stale <= 0)
    {
        console.log("cache item is stale. Contacting origin...");
        return [ true, false ];
    }

    return null;
}

function determine_is_immutable(response_cache_control_directives)
{
    const immutable_idx = response_cache_control_directives.findIndex(
        dir => dir.match(new RegExp(IMMUTABLE_DIRECTIVE)));
    return immutable_idx > -1;
}

function gotta_contact_origin(cached_response, client_request)
{
    // Requests with Range headers are simply passed through to the origin.
    // Other ways to handle Range requests would be:
    // - ignoring the Range header and replying with the whole body and a
    // 200 OK (allowed according to MDN)
    // - if the cache contains de requested document, the CDN could do craft
    // a reply with the requested range.
    if (client_request.headers.has(RANGE_HEADER)) { return [ true, false ]; }

    const response_cache_control_directives
        = cached_response.headers.get(CUSTOM_CACHE_CONTROL_HEADER)
              ?.replace(/\s/g, "")
              .split(",")
          || [];
    const request_cache_control_directives
        = client_request.headers.get(CUSTOM_CACHE_CONTROL_HEADER)
              ?.replace(/\s/g, "")
              .split(",")
          || [];

    // The goal is to stop evaluation of directives ASAP.
    // This means it's not possible to evaluate these functions in a loop,
    // as JS does not lazy evaluate function calls in an array :(
    let result = null;

    // `no-cache` and `immutable` are contradicting directives. Checking
    // whether the `immutable` directive applies is *not* done here, because
    // in accordance to our philosophy: `no-cache` and `immutable` are both
    // server directives, but `no-cache` wants to refresh (which we prefer)
    result = handle_no_cache(response_cache_control_directives);
    if (result) { return result; }

    const ms_before_stale = determine_ms_before_stale(cached_response);
    const is_immutable
        = determine_is_immutable(response_cache_control_directives);

    // check whether `immutable` directive applies *is* done here as
    // `immutable` is a server directive, which we prefer over the client
    // directive `min-fresh`
    if (!is_immutable)
    {
        result = handle_min_fresh(request_cache_control_directives,
                                  ms_before_stale);
        if (result) { return result; }
    }

    result = handle_must_revalidate(response_cache_control_directives,
                                    ms_before_stale);
    if (result) { return result; }

    result = handle_stale_while_revalidate(request_cache_control_directives,
                                           ms_before_stale);
    if (result) { return result; }

    result = handle_max_age(ms_before_stale);
    if (result) { return result; }

    // cache item is not stale and directives do not require a forced
    // refresh
    return [ false, false ];
}

function only_reply_with_cache(client_request)
{
    const request_cache_control_directives
        = client_request.headers.get(CUSTOM_CACHE_CONTROL_HEADER)
              ?.replace(/\s/g, "")
              .split(",")
          || [];
    const only_if_cached_idx = request_cache_control_directives.findIndex(
        dir => dir.match(new RegExp(ONLY_IF_CACHED_DIRECTIVE)));

    return only_if_cached_idx > -1;
}

function try_response_into_conditional(request, response)
{
    if (!(request.headers.has(IF_NONE_MATCH_HEADER)
          || request.headers.has(IF_MODIFIED_SINCE_HEADER)))
    {
        return response;
    }

    const response_cache_control_directives
        = response.headers.get(CUSTOM_CACHE_CONTROL_HEADER)
              ?.replace(/\s/g, "")
              .split(",")
          || [];
    if (determine_is_immutable(response_cache_control_directives))
    {
        return response;
    }

    // `If-None-Match` has precedence over `If-Modified-Since`
    const if_none_match = request.headers.get(IF_NONE_MATCH_HEADER)
                              ?.replace(/\s/g, "")
                              .replace(/W\//g, "")
                              .split(",")
                          || [];
    const if_modified_since
        = new Date(request.headers.get(IF_MODIFIED_SINCE_HEADER));
    const last_modified = new Date(response.headers.get(LAST_MODIFIED_HEADER));
    if (if_none_match.some(hash => hash === response.headers.get(E_TAG_HEADER))
        || last_modified < if_modified_since)
    {
        return new Response(
            null, { status : NOT_MODIFIED_STATUS, headers : response.headers });
    }
    else
    {
        return response;
    }
}

async function handle_warm_cache(
    cached_response, cache_key, url, client_request, event, cache)
{
    const [refresh, do_it_async]
        = gotta_contact_origin(cached_response, client_request);

    if (refresh)
    {
        // async updating of cache still replies with cache, so it's exempt
        // from the `only-if-cached` directive.
        if (do_it_async)
        {
            event.waitUntil(fetch_and_cache(
                url,
                client_request,
                event,
                cache,
                cache_key,
                cached_response,
                cached_response.headers.get(E_TAG_HEADER),
                cached_response.headers.get(LAST_MODIFIED_HEADER)));
        }
        else if (!only_reply_with_cache(client_request))
        {
            cached_response = await fetch_and_cache(
                url,
                client_request,
                event,
                cache,
                cache_key,
                cached_response,
                cached_response.headers.get(E_TAG_HEADER),
                cached_response.headers.get(LAST_MODIFIED_HEADER));
        }
        else
        {
            cached_response
                = new Response(null, { status : GATEWAY_TIMEOUT_STATUS });
        }
    }

    // In case the client's request was conditional, try answering
    // appropriately. This is done at the very end to ensure the CDN cache is as
    // close as possible to being a copy of the origin (under restrictions of
    // caching headers), before it answers conditional client requests.
    cached_response
        = try_response_into_conditional(client_request, cached_response);

    return cached_response;
}

async function handleRequest(event)
{
    let client_request = handle_edm_setup(event.request);

    const url = client_request.url.replace(WORKER_URL_REGEX, EDM_URL);
    console.log(url);

    const cache     = new ConnegCache(caches.default);
    const cache_key = url;

    let origin_response = await cache.match(cache_key, client_request);
    if (!origin_response)
    {
        console.log("Resource was not cached, fetching and then caching ",
                    cache_key);

        origin_response = await fetch_and_cache(
            url, client_request, event, cache, cache_key);
    }
    else
    {
        console.log("Resource was cached. Returning cached version", cache_key);

        // make mutable
        origin_response = new Response(origin_response.body, origin_response);

        origin_response = await handle_warm_cache(
            origin_response, cache_key, url, client_request, event, cache);
        origin_response.headers.set("WAS-CUSTOM-CACHED", cache_key);
    }

    // prevent browser from caching
    origin_response.headers.set(CACHE_CONTROL_HEADER, NO_STORE_DIRECTIVE);
    origin_response.headers.set(CACHE_CONTROL_HEADER.toLowerCase(),
                                NO_STORE_DIRECTIVE);

    return origin_response;
}
