module.exports = {
    root: true,
    env: {
      es6: true,
      node: true
    },
    extends: [
      "eslint:recommended",
    ],
    parserOptions: {
      ecmaVersion: 2020
    },
    rules: {
      "semi": [2, "always"],
      "camelcase": "off",
      "no-unused-vars": ["error", { "argsIgnorePattern": "^_" }],
    }
  };
