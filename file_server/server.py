from flask import Flask, request, send_from_directory
import mimetypes
import glob
import re
import os


ACCEPT_HEADER = "Accept"
ACCEPT_LANGUAGE_HEADER = "Accept-Language"
CUSTOM_VARY_HEADER = "Custom-Vary"


app = Flask(__name__)


def sort_accept_data(header_data):
    tuples = map(lambda tup: tup.split(";"),
                 header_data.replace(" ", "").split(","))
    item_with_quality = list(
        map(lambda tup: (tup[0], float(tup[1].split("=")[1]) if len(tup) == 2 else 1),
            tuples))
    # sort by quality 1st, original ordering 2nd
    return sorted(item_with_quality,
                  key=lambda tup: (tup[1], -item_with_quality.index(tup)), reverse=True)


@app.route("/<path:path>")
def handle_path(path):
    sorted_mime_tuples = []
    if ACCEPT_HEADER in request.headers:
        sorted_mime_tuples = sort_accept_data(
            request.headers.get(ACCEPT_HEADER))

    sorted_lang_tuples = []
    if ACCEPT_LANGUAGE_HEADER in request.headers:
        sorted_lang_tuples = sort_accept_data(
            request.headers.get(ACCEPT_LANGUAGE_HEADER))

    best_matching_file_path = path
    best_matching_score = 0

    # assume file names have syntax: <name>.<lang>.<ext>
    # it's not easy to determine whether the "something" in a request for `filename.something` is
    # an extension or language. We assume it to be the extension. If one wanted, this could be
    # checked by matching against a list of languages/extension.
    path_parts = path.split("/")
    file_parts = path_parts[-1].split(".")
    for file_path in glob.glob("./"+"/".join(path_parts[:-1])+"/"+file_parts[0]+"*"+".".join(file_parts[1:])):
        if not os.path.isfile(file_path):
            continue

        cur_score = 0

        file_type = mimetypes.guess_type(file_path)[0]
        file_type = "" if file_type is None else file_type
        file_language = file_path[2:].split(".")[1]

        for mime_type, score in sorted_mime_tuples:
            if len(re.findall(mime_type.replace("*", "\w*"), file_type)) > 0:
                cur_score += score
                break
        for lang, score in sorted_lang_tuples:
            if len(re.findall(lang.replace("*", "\w*"), file_language)) > 0:
                cur_score += score
                break

        if cur_score > best_matching_score:
            best_matching_score = cur_score
            best_matching_file_path = file_path

    response = send_from_directory("./", best_matching_file_path)
    response.headers[CUSTOM_VARY_HEADER] = ",".join(
        [ACCEPT_HEADER, ACCEPT_LANGUAGE_HEADER])

    return response


if __name__ == "__main__":
    app.run(port=29197)
